﻿using NUnit.Framework;

namespace Tests
{
    public class PassingTest
    {
        [Test]
        public void PassingTestSimplePasses()
        {
            Assert.Pass();
        }

        [Test]
        public void AnotherPassingTestSimplePasses()
        {
            Assert.Pass();
        }
    }
}
